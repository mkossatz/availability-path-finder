from typing import DefaultDict, Dict, List, Tuple
from collections import defaultdict
from dataclasses import dataclass
from enum import Enum


class AvailableDay(int):
    """Represents availability slots of a day.
    Availability slots are represented by a binary chain in integer format.
    A AvailableDay can contain multiple AvailableSegments.
    An AvailableSegment is a set of adjacent available time slots.
    Example AvailableSegment:   000000111000 == 56.
    Example AvailableDay:       001110111000 == 952.
    """


class WorkingDay(int):
    """Represents a block of slots a person should come to work.
    Working slots are represented by a binary chain in integer format.
    Example WorkingBlock: 001110111000 == 952.
    """


class StartSlotPosition(int):
    """Starting position of availability in binary representation.
    Example StartSlotPosition(4) refers to an availability ...X10000.
    """


class EndSlotPosition(int):
    """Ending position of availability in binary representation.
    Example EndSlotPosition(6) refers to an availability ...01XXXXX.
    """


class RemainingTimeSlots(int):
    """ """


AdjacentAvailableSegmentsType = DefaultDict[
    AvailableSegment,
    Dict[AvailableSegment, RemainingTimeSlots]
]


class SegmentDistance(int):
    """ """


class WeekDay(Enum):
    MON = 1
    TUE = 2
    WED = 3
    THU = 4
    FRI = 5

    def __gt__(self, other):
        if isinstance(other, WeekDay):
            return self.value > other.value
        return NotImplemented

    def __lt__(self, other):
        if isinstance(other, WeekDay):
            return self.value < other.value
        return NotImplemented


@dataclass(frozen=True)
class TimeTable:
    mon: AvailableDay
    tue: AvailableDay
    wed: AvailableDay
    thu: AvailableDay
    fri: AvailableDay


@dataclass(frozen=True)
class Person:
    uuid: int
    min_ws_per_week: int
    max_ws_per_week: int
    max_wt_per_week: int
    min_ws_per_day: int
    max_ws_per_day: int
    max_wt_per_day: int
    timetable: TimeTable


@dataclass(frozen=True)
class AvailableSegment:
    value: int
    start_slot_pos: StartSlotPosition
    end_slot_pos: EndSlotPosition
    person: Person
    week_day: WeekDay

    def __int__(self):
        return self.value

    def __lt__(self, other):
        if isinstance(other, AvailableSegment):
            return self.value < other.value
        return NotImplemented

    def __gt__(self, other):
        if isinstance(other, AvailableSegment):
            return self.value > other.value
        return NotImplemented


@dataclass(frozen=True)
class StartingAvailableSegment(AvailableSegment):
    value: int = 0
    start_slot_pos: StartSlotPosition = 0
    end_slot_pos: EndSlotPosition = 0
    person: Person = None
    week_day: WeekDay = WeekDay(1)


@dataclass(frozen=True)
class Availability:
    segments: List[AvailableSegment]
    segment_adjacencies: AdjacentAvailableSegmentsType
