import React from 'react';
import { range } from 'lodash';
import { Week } from './types';

export default function Calendar({ value, onChange }: { value: Week, onChange: (w: Week) => void }) {
    let [changeType, setChangeType] = React.useState<'set' | 'unset' | 'none'>('none');

    function setCell(day: number, hour: number, to: "0" | "1" | "flip") {
      let newWeek = [...value];
      let newDay = value[day].split("");
      newDay[hour] = to === "flip"
        ? (newDay[hour] === "0" ? "1" : "0")
        : to;
      newWeek[day] = newDay.join("");
      onChange(newWeek as Week);
    }

    return (
      <div className="table-wrap">
      <table className="calendar"><tbody>
        {range(0, 24).map(hour => (
          <tr key={hour}>
            {range(0, 7).map(day => (
              <td
                key={day}
                data-color={value[day][hour]}
                onMouseDown={event => {
                  event.preventDefault();

                  if (event.button === 0) {
                    setChangeType('set');
                    setCell(day, hour, "1");
                  }
                  else if (event.button === 2) {
                    setChangeType('unset');
                    setCell(day, hour, "0");
                  }
                  else {
                    setChangeType('none');
                  }
                }}
                onMouseEnter={event => {
                  if (changeType === 'set') {
                    setCell(day, hour, "1");
                  }
                  else if (changeType === 'unset') {
                    setCell(day, hour, "0");
                  }
                }}
                onMouseUp={() => setChangeType('none')}
                onContextMenu={event => event.preventDefault()}
              />
            ))}
          </tr>
        ))}
      </tbody></table>
      </div>
    );
  }
