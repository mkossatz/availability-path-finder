import React from 'react';

export default function IdInput({ value, onChange }: { value: string, onChange: (id: string) => void }) {
  let [id, setId] = React.useState(value);

  React.useEffect(() => {
    setId(value);
  }, [value]);

  return (
    <input
      value={id}
      onChange={event => setId(event.target.value)}
      onBlurCapture={event => onChange(id)}
    />
  );
}
