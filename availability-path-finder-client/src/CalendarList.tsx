import React from 'react';
import { stateContext, generateNewPerson } from './state';
import IdInput from './IdInput';

export default function CalendarList() {
    const [state, dispatch] = React.useContext(stateContext);

    return (
      <div className="list">
        {state.input.people.map((person, i) => (
          <div
            className="list-item"
            data-active={state.input.selected_person === i}
            key={"person-" + person.id}
            onClick={() => dispatch(state => {
              // this elements might have been deleted
              if (state.input.people[i] !== person) return state;

              return {
                ...state,
                input: {
                  ...state.input,
                  selected_person: i
                }
              };
            })}
          >
            <IdInput value={person.id} onChange={id => {
              dispatch(state => {
                // this elements might have been deleted
                if (state.input.people[i] !== person) return state;

                let newPeople = [...state.input.people];
                newPeople[i] = {
                  ...newPeople[i],
                  id,
                };

                return {
                  ...state,
                  input: {
                    ...state.input,
                    people: newPeople,
                  },
                };
              });
            }} />
            <button type="button" onClick={() => {
              dispatch(state => {
                let newPeople = [...state.input.people];
                newPeople.splice(i, 1);

                return {
                  ...state,
                  input: {
                    ...state.input,
                    people: newPeople,
                    selected_person: null,
                  },
                };
              });
            }}>DEL</button>
          </div>
        ))}
        <button type="button" onClick={() => {
          let newPeople = [...state.input.people, generateNewPerson()];

          dispatch(state => ({
            ...state,
            input: {
              ...state.input,
              people: newPeople,
            },
          }));
        }}>new person</button>
      </div>
    )
  }
