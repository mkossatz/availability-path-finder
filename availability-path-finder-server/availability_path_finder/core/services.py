from typing import List, Dict, Tuple
# from collections import deque, defaultdict
from . import models


def create_person(
    uuid: int,
    max_ws_pd: int, max_wt_pd: int,
    mon: str, tue: str,
    wed: str, thu: str, fri: str
) -> models.Person:
    # Todo: validate
    timetable = models.TimeTable(
        mon, tue, wed, thu, fri
    )
    person = models.Person(
        uuid, max_ws_pd, max_wt_pd,
        timetable
    )
    return person
