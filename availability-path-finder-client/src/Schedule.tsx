import React from 'react';
import { range } from 'lodash';
import { Person } from './types';

export default function Schedule({ people }: { people: Person[] }) {
  return (
    <div className="table-wrap">
    <table className="calendar">
    <tbody>
      {range(0, 24).map(hour => (
        <tr key={hour}>
          {range(0, 7).map(day => {
            let index = people.findIndex(p => p.availability[day][hour] === '1');

            if (index === -1) {
              return <td key={day} />;
            } else {
              let person = people[index];
              return <td title={person.id} key={day} data-color={String(index + 1)}/>;
            }
          })}
        </tr>
      ))}
    </tbody>
    </table>
    </div>
  );
}
