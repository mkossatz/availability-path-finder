
export function repeat<T>(thing: T, times: number): T[] {
  let out = [];
  for (let i = 0; i < times; i++) {
    out.push(thing);
  }
  return out;
}
