from typing import NamedTuple, List, Dict


class Time(str):
    """ """


class TimesAtWork(int):
    """ """


class HoursAtWork(int):
    """ """


class DayLength(int):
    """ """


class PersonUuid(int):
    """ """


class Timetable(NamedTuple):
    mon: Time
    tue: Time
    wed: Time
    thu: Time
    fri: Time


class Person(NamedTuple):
    uuid: PersonUuid
    max_wt_pd: TimesAtWork
    max_wh_pd: HoursAtWork
    timetable: Timetable


class SchedulePersonsRequest(NamedTuple):
    day_length: DayLength
    persons: List[Person]


class SchedulePersonsResponse(NamedTuple):
    mon: Dict[PersonUuid, Time]
    tue: Dict[PersonUuid, Time]
    wed: Dict[PersonUuid, Time]
    thu: Dict[PersonUuid, Time]
    fri: Dict[PersonUuid, Time]
