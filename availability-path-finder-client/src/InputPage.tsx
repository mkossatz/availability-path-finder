import React, { useContext } from 'react';
import CalendarList from './CalendarList';
import Calendar from './Calendar';
import { stateContext, STANDARD_WORK_HOURS } from './state';
import { PersonWithSettings, Person, Week } from './types';
import { repeat } from './utils';

async function calculate(people: PersonWithSettings[], workStartTime: number, workEndTime: number): Promise<Person[]> {
  return [{
    id: 'Tom',
    availability: repeat(STANDARD_WORK_HOURS, 7) as Week,
  }]
}

export default function InputPage({ setPage }: { setPage: (p: string) => void }) {
  let [state, dispatch] = useContext(stateContext);

  function calculateAndRedirect() {
    let realPeople = state.input.people.filter(p => p.id !== '_work');
    let work = state.input.people.find(p => p.id === '_work');

    if (work === undefined) {
      alert('_work calendar missing');
      return;
    }

    let workStartTime = Math.min(...work.availability.map(day => day.indexOf("1")));
    let workEndTime = Math.max(...work.availability.map(day => day.lastIndexOf("1")));

    if (workStartTime === -1) {
      workStartTime = 0;
    }
    if (workEndTime === -1) {
      workEndTime = 23;
    }

    calculate(realPeople, workStartTime, workEndTime)
      .then(people => {
        dispatch(state => ({
          ...state,
          output: {
            people,
          },
        }))
        setPage('output');
      })
      .catch(e => alert(e));
  }

  return (
    <>
      <button type="button" style={{position: 'fixed', right: 0, top: 0}} onClick={calculateAndRedirect}>
        calculate
      </button>
      <CalendarList />
      {state.input.selected_person !== null && (
        <Calendar
          value={state.input.people[state.input.selected_person].availability}
          onChange={week => dispatch(state => {
            if (state.input.selected_person === null) return state;

            let newPeople = [...state.input.people];
            newPeople[state.input.selected_person] = {
              ...newPeople[state.input.selected_person],
              availability: week,
            };

            return {
              ...state,
              input: {
                ...state.input,
                people: newPeople,
              },
            };
          })}
        />
      )}
    </>
  );
}
