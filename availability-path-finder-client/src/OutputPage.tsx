import React, { useContext } from 'react';
import Schedule from './Schedule';
import { stateContext } from './state';

export default function InputPage({ setPage }: { setPage: (p: string) => void }) {
  let [state, dispatch] = useContext(stateContext);

  return (
    <>
      <button type="button" style={{position: 'fixed', right: 0, top: 0}} onClick={() => setPage('input')}>
        back
      </button>
      <Schedule people={state.output.people} />
    </>
  );
}
