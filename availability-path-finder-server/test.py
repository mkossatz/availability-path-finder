import pytest

from availability_path_finder.core import services
from availability_path_finder.core import models
# import core.services

test_data = {
    "day_length": 12,
    "persons": [
        {
            "uuid": 1,
            "max_wt_pd": 1,
            "max_wh_pd": 2,
            "timetable": {
                "mon": "000000000011",
                "tue": "000000000011",
                "wed": "000000000011",
                "thu": "000000000011",
                "fri": "000000000011"
            }
        },
        {
            "uuid": 2,
            "max_wt_pd": 1,
            "max_wh_pd": 4,
            "timetable": {
                "mon": "001111011100",
                "tue": "001111011100",
                "wed": "001111011100",
                "thu": "001111011100",
                "fri": "001111011100"
            }
        },
        {
            "uuid": 3,
            "max_wt_pd": 1,
            "max_wh_pd": 2,
            "timetable": {
                "mon": "000000101100",
                "tue": "000000101100",
                "wed": "000000101100",
                "thu": "000000101100",
                "fri": "000000101100"
            }
        },
        {
            "uuid": 4,
            "max_wt_pd": 1,
            "max_wh_pd": 2,
            "timetable": {
                "mon": "000011110000",
                "tue": "000011110000",
                "wed": "000011110000",
                "thu": "000011110000",
                "fri": "000011110000"
            }
        },
        {
            "uuid": 5,
            "max_wt_pd": 1,
            "max_wh_pd": 2,
            "timetable": {
                "mon": "001100000000",
                "tue": "001100000000",
                "wed": "001100000000",
                "thu": "001100000000",
                "fri": "001100000000"
            }
        },
        {
            "uuid": 6,
            "max_wt_pd": 1,
            "max_wh_pd": 2,
            "timetable": {
                "mon": "110000000000",
                "tue": "110000000000",
                "wed": "110000000000",
                "thu": "110000000000",
                "fri": "110000000000"
            }
        }

    ]
}


def test_create_person():
    for person_data in test_data["persons"]:
        person = services.create_person(
            person_data["uuid"],
            person_data["max_wt_pd"],
            person_data["max_wh_pd"],
            person_data["timetable"]["mon"],
            person_data["timetable"]["tue"],
            person_data["timetable"]["wed"],
            person_data["timetable"]["thu"],
            person_data["timetable"]["fri"]
        )
        assert isinstance(person, models.Person)
