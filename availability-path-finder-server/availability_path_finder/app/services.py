from . import models as app_models
from ..core import models as core_models
from ..core import services as core_services


def schedule_persons(
    request: app_models.SchedulePersonsRequest
) -> app_models.SchedulePersonsResponse:
    for _person in request.persons:
        person = core_services.create_person(
            _person.uuid, _person.max_wh_pd, _person.max_wt_pd,
            _person.timetable.mon, _person.timetable.tue,
            _person.timetable.wed, _person.timetable.thu,
            _person.timetable.fri
        )
