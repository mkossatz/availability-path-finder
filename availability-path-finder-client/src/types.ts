
export type Week = [string, string, string, string, string, string, string];

export type Person = {
  id: string;
  availability: Week;
}

export type PersonWithSettings = Person & {
  max_time_a_day: number;
  max_time_a_week: number;
  max_shifts_a_day: number;
  max_shifts_a_week: number;
}

export type State = {
  input: {
    people: PersonWithSettings[];
    selected_person: number | null,
  };
  output: {
    people: Person[];
  };
}
