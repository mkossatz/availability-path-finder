import React, { useState, useReducer } from 'react';
import { stateContext, stateReducer, initialState } from './state';
import InputPage from './InputPage';
import OutputPage from './OutputPage';

import './App.css';

export default function App() {
  const [page, setPage] = useState('input');
  const [state, dispatch] = useReducer(stateReducer, initialState());

  return (
    <stateContext.Provider value={[state, dispatch]}>
      <div className="app">
        {(() => {
          switch (page) {
            case 'input': return <InputPage setPage={setPage} />;
            case 'output': return <OutputPage setPage={setPage} />;
            default: return '404, bitch';
          }
        })()}
      </div>
    </stateContext.Provider>
  );
}
