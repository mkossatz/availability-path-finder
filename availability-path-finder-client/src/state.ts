import React from 'react';
import { isEqual } from 'lodash';
import { Week, State, PersonWithSettings } from './types';
import { repeat } from './utils';

export const EMPTY_DAY = "0".repeat(24);
export const STANDARD_WORK_HOURS = "0".repeat(8) + "1".repeat(8) + "0".repeat(8);
export const GENERIC_STUDENT_DAY = "0".repeat(8) + "1".repeat(2) + "0" + "1".repeat(3) + "0".repeat(4) + "1".repeat(1) + "0".repeat(5);

export const DEFAULT_STATE: State = {
  input: {
    people: [{
      id: "_work",
      availability: repeat(STANDARD_WORK_HOURS, 7) as Week,
      max_time_a_day: 4,
      max_time_a_week: 20,
      max_shifts_a_day: 2,
      max_shifts_a_week: 10,
    }, {
      id: "Max",
      availability: repeat(GENERIC_STUDENT_DAY, 7) as Week,
      max_time_a_day: 4,
      max_time_a_week: 20,
      max_shifts_a_day: 2,
      max_shifts_a_week: 10,
    }],
    selected_person: 0,
  },
  output: {
    people: [],
  }
};

export const stateContext = React.createContext([DEFAULT_STATE, () => DEFAULT_STATE] as [State, (action: (s: State) => State) => void]);

let savedState: State | undefined = undefined;

export function autoSaveState(interval: number) {
  window.setInterval(() => {
    if (savedState === undefined) return;

    let stored = window.localStorage.getItem("state") || "null";

    let oldState = JSON.parse(stored);
    if (isEqual(oldState, savedState)) return;

    window.localStorage.setItem("state", JSON.stringify(savedState));
  }, interval);
}

export function saveState(newState: State) {
  savedState = newState;
}

export function stateReducer(state: State, action: (s: State) => State): State {
  savedState = action(state);
  return savedState;
}

export function initialState(): State {
  let saved = window.localStorage.getItem("state");

  return saved !== null
    ? JSON.parse(saved)
    : DEFAULT_STATE;
}

export function generateNewPerson(): PersonWithSettings {
  return {
    id: String(Math.floor(Math.random() * 1000)),
    availability: repeat(GENERIC_STUDENT_DAY, 7) as Week,
    max_time_a_day: 4,
    max_time_a_week: 20,
    max_shifts_a_day: 2,
    max_shifts_a_week: 10,
  };
}
